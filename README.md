# README #
### Project description ###

Demonstrative project which provides 4up imposition page generation for printing.

### How do I get set up? ###

```bash
# 1. Ensure you have the correct node/npm versions node v14.16.1 (npm v6.14.12)
$ nvm use --lts
# 2. Install node_modules
$ npm i

# 3. Starting the application
$ npm start

# optional commands
$ npm run lint
$ npm run typecheck
$ npm run stylelint
$ npm run test
```


### Who do I talk to? ###

* Email: alexander.leismann@tptlive.ee
