import { yupResolver } from '@hookform/resolvers/yup';
import React from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { getPrintImpositionPages } from 'helpers/printer';

import styles from './Form.module.scss';

interface FormData {
  pageCount: string;
}

const Form: React.FC = () => {
  const {
    register, handleSubmit, formState: { errors, isDirty },
  } = useForm<FormData>({
    resolver: yupResolver(yup.object().shape({
      pageCount: yup.number().min(1).required(),
    })),
  });

  const getImpositation = (data: FormData): void => {
    alert(`Page numbers are the following: ${getPrintImpositionPages(Number(data.pageCount))}`);
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(getImpositation)}>
      <label className={styles.label}>
        <div className={styles.labelText}>
          Page count*
        </div>

        <input
          className={styles.input}
          type="number"
          min={1}
          {...register('pageCount')}
        />
        <div className={styles.inputError}>{errors.pageCount?.message}</div>
      </label>

      <div className={styles.buttonContainer}>
        <button type="submit" disabled={!isDirty}>
          update
        </button>
      </div>
    </form>
  );
};

export default Form;
