import React from 'react';

import Form from './Form';

import styles from './App.module.scss';

const App: React.FC = () => (
<div className={styles.container}>
    <h1 className={styles.h1}>4up booklet imposition calculation</h1>

    <Form />
</div>
);

export default App;
