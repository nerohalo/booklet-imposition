import { getPrintImpositionPages } from './index';

describe('getPrintImpositionPages', () => {
  test('gets correct imposed pages for 16 pages ', () => {
    expect(getPrintImpositionPages(16)).toEqual('12,5,16,1,6,11,2,15,10,7,14,3,8,9,4,13');
  });

  test('gets correct imposed pages for 15 pages ', () => {
    expect(getPrintImpositionPages(15)).toEqual('12,5,16,1,6,11,2,15,10,7,14,3,8,9,4,13');
  });

  test('generates correct amount of blank pages if inserted pages are not divisible by 4', () => {
    expect(getPrintImpositionPages(13)).toEqual('12,5,16,1,6,11,2,15,10,7,14,3,8,9,4,13');
  });

  test('gets correct imposed pages for 8 pages ', () => {
    expect(getPrintImpositionPages(8)).toEqual('6,3,8,1,4,5,2,7');
  });

  test('gets correct imposed pages for 8 pages ', () => {
    expect(getPrintImpositionPages(7)).toEqual('6,3,8,1,4,5,2,7');
  });
});
