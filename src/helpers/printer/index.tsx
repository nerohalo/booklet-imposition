/* eslint-disable no-plusplus */
/**
 * A method for generating imposition pages for printing
 * @param pages – pages to be printed
 * @returns string of imposition pages for printing 12,5,16,1...
 */

export const getPrintImpositionPages = (pages: number): string => {
  if (pages === 0) {
    throw new Error('Cannot call getPrintImpositionPages with value 0.');
  }

  let totalPages = pages;

  // Adds missing pages for a 4 page layout on the sheet
  if (pages % 4 !== 0) {
    totalPages += 4 - (pages % 4);
  }

  // Generate array of page numbers
  // example [1,2,3,4,5,6...]

  const array = [...new Array(totalPages)].map((_, i) => i + 1);

  // Generate array containing subArrays of booklet pages, uses double pointer approach
  // example [[16, 1], [15, 2], [14, 3], [13, 4], [12, 5]...]

  let subArrays = [];

  for (let i = 0; i < array.length / 2; i++) {
    subArrays[i] = [array[i], array[(array.length - 1) - i]];
  }

  // Reverses the order of each nonOdd subArray
  // example [[16, 1], [15, 2], [14, 3], [13, 4], [12, 5]...]

  subArrays = subArrays.map((arr, index) => (index % 2 ? arr : arr.reverse()));

  // Generates the final result array, uses double pointer approach
  // we split the array into the upper page subArrays and lower page subArrays by splitting in the middle and add them one after another.
  // example [...], [...] | [...], [...]

  const result = [];

  for (let i = 0; i < subArrays.length / 2; i++) {
    result.push(subArrays[((subArrays.length) / 2) + i]);
    result.push(subArrays[i]);
  }

  // Finally we return the array converted to string;

  return result.toString();
};
